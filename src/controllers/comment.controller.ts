import { Request, Response } from "express";
import { getRepository, Repository } from "typeorm";
import { Comment } from "../entity/Comment";
import UtilResponse from "../utils/response.util";

export default class CommentsController {
  private repository: Repository<Comment>;

  constructor() {
    this.repository = getRepository(Comment);
  }

  public async createComment(request: Request, response: Response) {
    try {
      const comment = this.repository.create(request.body);
      const result = await this.repository.save(comment);
      return response.json(
        UtilResponse.buildResponse(
          `new comment was created for the post ${request.body.post}`,
          result
        )
      );
    } catch (exception) {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `Error <${exception}>, creating comment for post: <${request.body.post}>`
          )
        );
    }
  }

  public async getCommentsForPost(request: Request, response: Response) {
    try {
      const { idPost } = request.params;
      const comments = await this.repository.find({
        where: {
          post: idPost,
        },
      });
      return response.json(
        UtilResponse.buildResponse(
          `comments were obntained <${comments.length}> for the post <${idPost}>`,
          comments
        )
      );
    } catch (exception) {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `an error occurred <${exception}>, obtaining comments for the post with id:  ${request.params.idPost}`,
            null
          )
        );
    }
  }
}
