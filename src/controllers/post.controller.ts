import { Request, Response } from "express";
import { getRepository, Repository } from "typeorm";
import { Post } from "../entity/Post";
import ReactionUseCaseImpl from "../use-cases/impl/ReactionPost.UseCaseImpl";
import UtilResponse from "../utils/response.util";

export default class PostController {
  private repository: Repository<Post>;
  private reactionPostUseCase: ReactionUseCaseImpl;

  constructor() {
    this.repository = getRepository(Post);
    this.reactionPostUseCase = new ReactionUseCaseImpl(this.repository);
  }

  public async getPosts(
    request: Request,
    response: Response
  ): Promise<Response> {
    try {
      const take: number =
        typeof request.query.take === "string"
          ? Number(request.query.take)
          : 10;
      const skip: number =
        typeof request.query.skip === "string" ? Number(request.query.skip) : 1;
      const page: number =
        typeof request.query.page === "string" ? Number(request.query.page) : 1;

      const [posts, total] = await this.repository.findAndCount({
        take,
        skip,
        order: {
          createdDate: "DESC",
        },
        relations: ["comments"],
      });
      const paginationResponse = UtilResponse.buildPaginateResponse(
        total,
        page,
        take
      );
      return response.json(
        UtilResponse.buildResponse(
          `${posts.length} posts were obtained , total ${total}`,
          { posts, paginationResponse }
        )
      );
    } catch {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `Error save post for user: <${request.body.emailCreator}>`
          )
        );
    }
  }

  public async getOnePost(request: Request, response: Response) {
    try {
      const { id } = request.params;
      const post = await this.repository.find({
        where: {
          id,
        },
        relations: ['comments']
      });
      return response.json(
        UtilResponse.buildResponse(`get post <${id}>`, post[0])
      );
    } catch (exception) {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `an error occurred <${exception}>, obtaining post with id:  ${request.params.id}`,
            null
          )
        );
    }
  }

  public async createPost(
    request: Request,
    response: Response
  ): Promise<Response> {
    try {
      var post : Post | Post [] = this.repository.create(request.body);
      const result: Post | Post [] = await this.repository.save(post);
      if((result instanceof Post)){
        post = await this.repository.find({
          where: {
            id: result.id,
          },
          relations: ['comments']
        });
      }
      return response.json(
        UtilResponse.buildResponse(
          `new post was created for the user ${request.body.emailCreator}`,
          post
        )
      );
    } catch (exception) {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `Error <${exception}>, creating post for user: <${request.body.emailCreator}>`
          )
        );
    }
  }

  public async reactionPost(
    request: Request,
    response: Response
  ): Promise<Response> {
    try {
      const { id } = request.params;
      const reactionType: ReactionTypes = request.body.type;
      const reactionAction: ReactionAction = request.body.action;
      if (
        !(await this.reactionPostUseCase.executeReaction(
          id,
          reactionType,
          reactionAction
        ))
      ) {
        throw new Error(`Post with Id <${id}> not found`);
      }
      return response.json(
        UtilResponse.buildResponse(
          `updated the post with id <${id}>, ${reactionAction}1 <${reactionType}>!`
        )
      );
    } catch (exception) {
      return response
        .status(500)
        .json(
          UtilResponse.buildResponse(
            `an error occurred <${exception}>, updating the post with id:  ${request.params.id}`,
            null
          )
        );
    }
  }
}
