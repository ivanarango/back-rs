import {Router} from 'express';
import CommentsController from '../controllers/comment.controller';
import CreateCommentValidator from '../middlewares/validators/CreateComment.validator';
import ValidationSchemas from '../middlewares/validators/ValidationSchemas';

export default class CommentRouter { 

    public router : Router;
    public controller: CommentsController;

    constructor(){
        this.router = Router();
        this.controller = new CommentsController();
        this.setupRoutes();
    }


    private setupRoutes(): void {
        this.router.get('/:idPost',this.controller.getCommentsForPost.bind(this.controller));
        this.router.post('/',CreateCommentValidator,ValidationSchemas,this.controller.createComment.bind(this.controller));
    }

}