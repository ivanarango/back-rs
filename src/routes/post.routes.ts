import { Router } from "express";
import PostController from "../controllers/post.controller";
import CreatePostValidator from "../middlewares/validators/CreatePost.validator";
import ReactPostValidator from "../middlewares/validators/ReactPost.validator";
import ValidationSchemas from "../middlewares/validators/ValidationSchemas";

export default class PostRouter {
  public router: Router;
  public controller: PostController;

  constructor() {
    this.router = Router();
    this.controller = new PostController();
    this.setupRoutes();
  }
  private setupRoutes(): void {
    this.router.get("/", this.controller.getPosts.bind(this.controller));
    this.router.get("/:id", this.controller.getOnePost.bind(this.controller));
    this.router.post(
      "/",
      CreatePostValidator,
      ValidationSchemas,
      this.controller.createPost.bind(this.controller)
    );
    this.router.put(
      "/:id",
      ReactPostValidator,
      ValidationSchemas,
      this.controller.reactionPost.bind(this.controller)
    );
  }
}
