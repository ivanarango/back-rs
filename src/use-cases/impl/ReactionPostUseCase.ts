export interface ReactionPostUseCase {
  executeReaction(
    idPost: string,
    reactionType: ReactionTypes,
    reactionAction: ReactionAction
  ): Promise<boolean>;
}
