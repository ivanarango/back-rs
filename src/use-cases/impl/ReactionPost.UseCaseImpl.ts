import { Repository } from "typeorm";
import { Post } from "../../entity/Post";
import { ReactionPostUseCase } from "./ReactionPostUseCase";

export default class ReactionUseCaseImpl implements ReactionPostUseCase {
  private repository: Repository<Post>;

  constructor(repository: Repository<Post>) {
    this.repository = repository;
  }

  public async executeReaction(
    idPost: string,
    reactionType: ReactionTypes,
    reactionAction: ReactionAction
  ): Promise<boolean> {
    try {
      const result = await this.repository
        .createQueryBuilder()
        .update()
        .set({
          likes: () =>
            ReactionTypes.LIKE === reactionType
              ? `likes ${reactionAction} 1`
              : `likes`,
          dislikes: () =>
            ReactionTypes.DISLIKE === reactionType
              ? `dislikes ${reactionAction} 1`
              : `dislikes`,
        })
        .whereInIds(idPost)
        .execute();
      return result.affected != undefined && result.affected > 0;
    } catch (exception) {
      throw new Error(`Error adding reaction to post with id <${idPost}>`);
    }
  }
}
