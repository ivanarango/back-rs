import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Post } from "./Post";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
  })
  emailCreator: string;

  @Column({
    nullable: false,
  })
  commentName: string;

  @Column({
    nullable: false,
  })
  content: string;

  @CreateDateColumn()
  createdDate: Date;

  @ManyToOne(() => Post, (post) => post.comments, { nullable: false })
  post: Post;
}
