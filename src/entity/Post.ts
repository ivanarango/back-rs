import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Comment } from "./Comment";

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
  })
  postName: string;

  @Column({
    nullable: false,
  })
  content: string;

  @CreateDateColumn()
  createdDate: Date;

  @Column()
  likes: number;

  @Column()
  dislikes: number;

  @Column({
    nullable: false,
  })
  emailCreator: string;

  @OneToMany(() => Comment, (comment) => comment.post, { cascade: true })
  comments: Comment[];
}
