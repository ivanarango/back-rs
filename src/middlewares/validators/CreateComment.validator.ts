import { checkSchema } from "express-validator";

export default checkSchema({
  commentName: {
    in: ["body"],

    exists: {
      errorMessage: "commentName is required",
    },

    isString: {
      errorMessage: `commentName must be a string`,
    },

    isLength: {
      errorMessage:
        "commentName must have a maximum of 255 characters and a minimum of 1",
      options: { max: 255, min: 1 },
    },
  },

  content: {
    in: ["body"],

    exists: {
      errorMessage: "content is required",
    },

    isString: {
      errorMessage: `content must be a string`,
    },

    isLength: {
      errorMessage:
        "content must have a maximum of 255 characters and a minimum of 1",
      options: { max: 255, min: 1 },
    },
  },

  post: {
    in: ["body"],

    exists: {
      errorMessage: "post is required",
    },

    isNumeric: {
      errorMessage: `post must be a number`,
    },
  },
});
