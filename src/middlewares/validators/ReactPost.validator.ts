import { checkSchema } from "express-validator";

export default checkSchema({
  type: {
    in: ["body"],

    exists: {
      errorMessage: "type is required",
    },

    isString: {
      errorMessage: `type must be a string`,
    },

    custom: {
      options: (type) => type === `LIKE` || type === `DISLIKE`,
      errorMessage: `the value of type is not allowed`,
    },
  },

  action: {
    in: ["body"],

    exists: {
      errorMessage: "action is required",
    },

    isString: {
      errorMessage: `action must be a string`,
    },

    custom: {
      options: (type) => type === `+` || type === `-`,
      errorMessage: `the value of action is not allowed`,
    },
  },
});
