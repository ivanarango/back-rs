import { checkSchema } from "express-validator";

export default checkSchema({
  postName: {
    in: ["body"],

    exists: {
      errorMessage: "postName is required",
    },

    isString: {
      errorMessage: `postName must be a string`,
    },

    isLength: {
      errorMessage:
        "postName must have a maximum of 255 characters and a minimum of 1",
      options: { max: 255, min: 1 },
    },
  },

  content: {
    in: ["body"],

    exists: {
      errorMessage: "content is required",
    },

    isString: {
      errorMessage: `content must be a string`,
    },

    isLength: {
      errorMessage:
        "content must have a maximum of 255 characters and a minimum of 1",
      options: { max: 255, min: 1 },
    },
  },

  emailCreator: {
    in: ["body"],

    exists: {
      errorMessage: "emailCreator is required",
    },

    isString: {
      errorMessage: `emailCreator must be a string`,
    },

    isLength: {
      errorMessage:
        "emailCreator must have a maximum of 255 characters and a minimum of 1",
      options: { max: 255, min: 1 },
    },
  },
});
