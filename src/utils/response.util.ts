import { ResponseApi } from "../dtos/web/response/ResponseApi";

export default class UtilResponse {
  static buildResponse(message: string, data?: any): ResponseApi<any> {
    const resBuild = new ResponseApi<any>();
    resBuild.message = message;
    resBuild.data = data;
    return resBuild;
  }

  static buildPaginateResponse(total: number, page: number, limit: number) {
    const lastPage = Math.ceil(total / limit);
    const nextPage = page + 1 > lastPage ? null : page + 1;
    const prevPage = page - 1 < 1 ? null : page - 1;
    return {
      total,
      currentPage: page,
      nextPage,
      prevPage,
      lastPage,
    };
  }
}
